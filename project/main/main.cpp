#include <SDL.h>
#include <cstddef>
#include <sstream>
#include <iostream>

#include <SDL_vulkan.h>
#include <vulkan/vulkan.h>

#include "Core/List.hpp"
#include "Core/String.hpp"
#include "Core/File.hpp"

bool running{ true };

void runMainLoop()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type) {
		case SDL_QUIT:
			running = false;
			break;
		case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE)running = false;
			break;
		}
	}
}

void check_vk_result(VkResult err) {
	if (err == 0) return;
	fprintf(stderr, "[vulkan] Error: VkResult = %d\n", err);
}

void check(bool condition) {
	if (!condition)
		SDL_Log("Assertion Failed");
}

struct VkInfo {
	VkInstance hInstance;
	VkPhysicalDevice hPhysicalDevice;
	VkDevice hDevice;
	VkQueue hQueue;
	int32_t queueFamilyIndex;
	VkDescriptorPool hPool;
	VkSurfaceKHR hSurface;
	VkSwapchainKHR hSwapChain;
};

void SetupVulkan(
	const char** extensions,
	uint32_t extensions_count,
	VkInfo* pInfo)
{

#ifdef __ANDROID__NO
	// Android Vulkan Loader
	check(InitVulkan());
#endif

	VkResult err;

	{
		VkInstanceCreateInfo create_info = {};
		create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		create_info.enabledExtensionCount = extensions_count;
		create_info.ppEnabledExtensionNames = extensions;
		// Create Vulkan Instance without any debug feature
		err = vkCreateInstance(&create_info, nullptr, &pInfo->hInstance);
		check_vk_result(err);
	}

	// Select GPU
	{
		uint32_t gpu_count;
		err = vkEnumeratePhysicalDevices(pInfo->hInstance, &gpu_count, nullptr);
		check_vk_result(err);
		check(gpu_count > 0);

		VkPhysicalDevice* gpus = new VkPhysicalDevice[gpu_count];
		err = vkEnumeratePhysicalDevices(pInfo->hInstance, &gpu_count, gpus);
		check_vk_result(err);

		// If a number >1 of GPUs got reported, find discrete GPU if present, or use first one available. This covers
		// most common cases (multi-gpu/integrated+dedicated graphics). Handling more complicated setups (multiple
		// dedicated GPUs) is out of scope of this sample.
		int use_gpu = 0;
		for (int i = 0; i < (int)gpu_count; i++)
		{
			VkPhysicalDeviceProperties properties;
			vkGetPhysicalDeviceProperties(gpus[i], &properties);
			if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			{
				use_gpu = i;
				break;
			}
		}

		pInfo->hPhysicalDevice = gpus[use_gpu];
		delete[] gpus;
	}

	pInfo->queueFamilyIndex = -1;
	// Select graphics queue family
	{
		uint32_t count;
		vkGetPhysicalDeviceQueueFamilyProperties(pInfo->hPhysicalDevice, &count, nullptr);
		VkQueueFamilyProperties* queues = new VkQueueFamilyProperties[count];
		vkGetPhysicalDeviceQueueFamilyProperties(pInfo->hPhysicalDevice, &count, queues);
		for (uint32_t i = 0; i < count; i++)
			if (queues[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				pInfo->queueFamilyIndex = i;
				break;
			}
		delete[] queues;
		check(pInfo->queueFamilyIndex != (uint32_t)-1);
	}

	// Create Logical Device (with 1 queue)
	{
		uint32_t layersCount{};
		vkEnumerateInstanceLayerProperties(&layersCount, nullptr);
		VkLayerProperties* layers = new  VkLayerProperties[layersCount];
		vkEnumerateInstanceLayerProperties(&layersCount, layers);
		for (int i = 0; i < layersCount; i++) {
			SDL_Log("%s", layers[i].layerName);
		}
		delete[] layers;

		int device_extension_count = 1;
		const char* device_extensions[] = { "VK_KHR_swapchain" };
		int enableLayerCount = 1;
		const char* enableLayerNames[] = { "VK_KHR_swapchain" };

		const float queue_priority[] = { 1.0f };
		VkDeviceQueueCreateInfo queue_info[1] = {};
		queue_info[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queue_info[0].queueFamilyIndex = pInfo->queueFamilyIndex;
		queue_info[0].queueCount = 1;
		queue_info[0].pQueuePriorities = queue_priority;
		VkDeviceCreateInfo create_info = {};
		create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		create_info.queueCreateInfoCount = sizeof(queue_info) / sizeof(queue_info[0]);
		create_info.pQueueCreateInfos = queue_info;
		create_info.enabledExtensionCount = device_extension_count;
		create_info.ppEnabledExtensionNames = device_extensions;
		create_info.enabledLayerCount = enableLayerCount;
		create_info.ppEnabledLayerNames = enableLayerNames;
		err = vkCreateDevice(pInfo->hPhysicalDevice, &create_info, nullptr, &pInfo->hDevice);
		check_vk_result(err);
		vkGetDeviceQueue(pInfo->hDevice, pInfo->queueFamilyIndex, 0, &pInfo->hQueue);
	}

	// Create Descriptor Pool
	{
		const uint32_t pool_sizes_len = 11;
		VkDescriptorPoolSize pool_sizes[] =
		{
			{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
			{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
			{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
			{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
			{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
			{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
		};
		VkDescriptorPoolCreateInfo pool_info = {};
		pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		pool_info.maxSets = 1000 * pool_sizes_len;
		pool_info.poolSizeCount = pool_sizes_len;
		pool_info.pPoolSizes = pool_sizes;
		err = vkCreateDescriptorPool(pInfo->hDevice, &pool_info, nullptr, &pInfo->hPool);
		check_vk_result(err);
	}
}

void SetupVulkan(SDL_Window* pWindow, VkInfo* pInfo) {
	VkResult err;
	uint32_t extensions_count = 0;
	SDL_Vulkan_GetInstanceExtensions(pWindow, &extensions_count, nullptr);
	const char** extensions = new const char* [extensions_count];
	SDL_Vulkan_GetInstanceExtensions(pWindow, &extensions_count, extensions);

	SetupVulkan(extensions, extensions_count, pInfo);

	SDL_Vulkan_CreateSurface(pWindow, pInfo->hInstance, &pInfo->hSurface);

	uint32_t supported{};
	vkGetPhysicalDeviceSurfaceSupportKHR(pInfo->hPhysicalDevice, pInfo->queueFamilyIndex, pInfo->hSurface, &supported);
	check(supported);

	VkSurfaceCapabilitiesKHR hSurfaceCapabilities{};
	err = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pInfo->hPhysicalDevice, pInfo->hSurface, &hSurfaceCapabilities);
	check_vk_result(err);

	uint32_t presentationModeCount{};
	err = vkGetPhysicalDeviceSurfacePresentModesKHR(pInfo->hPhysicalDevice, pInfo->hSurface, &presentationModeCount, nullptr);
	check_vk_result(err);
	check(presentationModeCount > 0);
	VkPresentModeKHR* presentationModes = new VkPresentModeKHR[presentationModeCount];
	err = vkGetPhysicalDeviceSurfacePresentModesKHR(pInfo->hPhysicalDevice, pInfo->hSurface, &presentationModeCount, presentationModes);
	check_vk_result(err);

	VkSwapchainCreateInfoKHR swapChainCreateInfo{};
	swapChainCreateInfo.pNext = nullptr;
	swapChainCreateInfo.flags = 0;
	swapChainCreateInfo.surface = pInfo->hSurface;
	swapChainCreateInfo.minImageCount = 2;
	swapChainCreateInfo.imageFormat = VK_FORMAT_B8G8R8A8_UNORM;
	swapChainCreateInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	swapChainCreateInfo.imageExtent = hSurfaceCapabilities.currentExtent;
	swapChainCreateInfo.imageArrayLayers = 1;
	swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapChainCreateInfo.queueFamilyIndexCount = 0;
	swapChainCreateInfo.pQueueFamilyIndices = nullptr;
	swapChainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapChainCreateInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR;
	swapChainCreateInfo.clipped = true;
	swapChainCreateInfo.oldSwapchain = nullptr;
	swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;

	err = vkCreateSwapchainKHR(pInfo->hDevice, &swapChainCreateInfo, nullptr, &pInfo->hSwapChain);
	check_vk_result(err);

	uint32_t swapChainImageCount{};
	err = vkGetSwapchainImagesKHR(pInfo->hDevice, pInfo->hSwapChain, &swapChainImageCount, nullptr);
	check_vk_result(err);
	VkImage* swapChainImages = new VkImage[swapChainImageCount];
	err = vkGetSwapchainImagesKHR(pInfo->hDevice, pInfo->hSwapChain, &swapChainImageCount, swapChainImages);
	check_vk_result(err);

	VkImageView* swapChainImageViews = new VkImageView[swapChainImageCount];
	for (int i = 0; i < swapChainImageCount; i++) {

		VkImageViewCreateInfo imageViewCreateInfo{};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.image = swapChainImages[i];
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = swapChainCreateInfo.imageFormat;
		imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;

		err = vkCreateImageView(pInfo->hDevice, &imageViewCreateInfo, nullptr, &swapChainImageViews[i]);
		check_vk_result(err);
	}

	VkAttachmentDescription attachmentDescriptor{};
	attachmentDescriptor.format = swapChainCreateInfo.imageFormat;                                  // Use the color format selected by the swapchain
	attachmentDescriptor.samples = VK_SAMPLE_COUNT_1_BIT;                                 // We don't use multi sampling in this example
	attachmentDescriptor.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;                            // Clear this attachment at the start of the render pass
	attachmentDescriptor.storeOp = VK_ATTACHMENT_STORE_OP_STORE;                          // Keep its contents after the render pass is finished (for displaying it)
	attachmentDescriptor.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;                 // We don't use stencil, so don't care for load
	attachmentDescriptor.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;               // Same for store
	attachmentDescriptor.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;                       // Layout at render pass start. Initial doesn't matter, so we use undefined
	attachmentDescriptor.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference attachmentReference{};
	attachmentReference.attachment = 0;
	attachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpassDescription{};
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = 1;
	subpassDescription.pColorAttachments = &attachmentReference;

	VkRenderPassCreateInfo renderPassCreateInfo{};
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.attachmentCount = 1;
	renderPassCreateInfo.pAttachments = &attachmentDescriptor;
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpassDescription;

	VkRenderPass renderPass{};
	err = vkCreateRenderPass(pInfo->hDevice, &renderPassCreateInfo, nullptr, &renderPass);
	check_vk_result(err);

	{
		try {
			String shaderVert("assets/shaders/shader.vert");
			List<byte> shaderVertCode{};
			File::Read(&shaderVert, &shaderVertCode);

			VkShaderModuleCreateInfo shaderModuleCreateInfo{};
			shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
			shaderModuleCreateInfo.codeSize = shaderVertCode.Size();
			shaderModuleCreateInfo.pCode = (uint32_t*)shaderVertCode.Elements();
			VkShaderModule shaderModule{};
			err = vkCreateShaderModule(pInfo->hDevice, &shaderModuleCreateInfo, nullptr, &shaderModule);
			check_vk_result(err);
			VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo{};
			pipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			pipelineShaderStageCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
			pipelineShaderStageCreateInfo.module = shaderModule;
			pipelineShaderStageCreateInfo.pName = "main";




			VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
			vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			vertexInputInfo.vertexBindingDescriptionCount = 0;
			vertexInputInfo.pVertexBindingDescriptions = nullptr; // Optional
			vertexInputInfo.vertexAttributeDescriptionCount = 0;
			vertexInputInfo.pVertexAttributeDescriptions = nullptr; // Optional
			VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
			inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			inputAssembly.primitiveRestartEnable = VK_FALSE;
			VkViewport viewport{};
			viewport.x = 0.0f;
			viewport.y = 0.0f;
			viewport.width = (float)hSurfaceCapabilities.currentExtent.width;
			viewport.height = (float)hSurfaceCapabilities.currentExtent.height;
			viewport.minDepth = 0.0f;
			viewport.maxDepth = 1.0f;
			VkRect2D scissor{};
			scissor.offset = { 0, 0 };
			scissor.extent = hSurfaceCapabilities.currentExtent;
			VkPipelineViewportStateCreateInfo viewportState{};
			viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
			viewportState.viewportCount = 1;
			viewportState.pViewports = &viewport;
			viewportState.scissorCount = 1;
			viewportState.pScissors = &scissor;
			VkPipelineRasterizationStateCreateInfo rasterizer{};
			rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			rasterizer.depthClampEnable = VK_FALSE;
			rasterizer.rasterizerDiscardEnable = VK_FALSE;
			rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
			rasterizer.lineWidth = 1.0f;
			rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
			rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
			rasterizer.depthBiasEnable = VK_FALSE;
			rasterizer.depthBiasConstantFactor = 0.0f; // Optional
			rasterizer.depthBiasClamp = 0.0f; // Optional
			rasterizer.depthBiasSlopeFactor = 0.0f; // Optional
			VkPipelineMultisampleStateCreateInfo multisampling{};
			multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			multisampling.sampleShadingEnable = VK_FALSE;
			multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			multisampling.minSampleShading = 1.0f; // Optional
			multisampling.pSampleMask = nullptr; // Optional
			multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
			multisampling.alphaToOneEnable = VK_FALSE; // Optional
			VkPipelineColorBlendAttachmentState colorBlendAttachment{};
			colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
			colorBlendAttachment.blendEnable = VK_FALSE;
			colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
			colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
			colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
			colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
			colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
			colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional
			VkPipelineColorBlendStateCreateInfo colorBlending{};
			colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			colorBlending.logicOpEnable = VK_FALSE;
			colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
			colorBlending.attachmentCount = 1;
			colorBlending.pAttachments = &colorBlendAttachment;
			colorBlending.blendConstants[0] = 0.0f; // Optional
			colorBlending.blendConstants[1] = 0.0f; // Optional
			colorBlending.blendConstants[2] = 0.0f; // Optional
			colorBlending.blendConstants[3] = 0.0f; // Optional
			VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
			pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			pipelineLayoutInfo.setLayoutCount = 0; // Optional
			pipelineLayoutInfo.pSetLayouts = nullptr; // Optional
			pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
			pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional
			
			VkPipelineLayout pipelineLayout;
			err = vkCreatePipelineLayout(pInfo->hDevice, &pipelineLayoutInfo, nullptr, &pipelineLayout);
			check_vk_result(err);


			VkGraphicsPipelineCreateInfo pipelineInfo{};
			pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			pipelineInfo.stageCount = 1;
			pipelineInfo.pStages = &pipelineShaderStageCreateInfo;
			pipelineInfo.pVertexInputState = &vertexInputInfo;
			pipelineInfo.pInputAssemblyState = &inputAssembly;
			pipelineInfo.pViewportState = &viewportState;
			pipelineInfo.pRasterizationState = &rasterizer;
			pipelineInfo.pMultisampleState = &multisampling;
			pipelineInfo.pDepthStencilState = nullptr; // Optional
			pipelineInfo.pColorBlendState = &colorBlending;
			pipelineInfo.pDynamicState = nullptr; // Optional
			pipelineInfo.layout = pipelineLayout;
			pipelineInfo.renderPass = renderPass;
			pipelineInfo.subpass = 0;
			pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
			pipelineInfo.basePipelineIndex = -1; // Optional

			VkPipeline graphicsPipeline;
			err = vkCreateGraphicsPipelines(pInfo->hDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline);
			check_vk_result(err);


		}
		catch (Exception& ex) {
			SDL_Log("%s", ex.what());
		}
	}




	delete[] extensions;
}

void CleanupVulkan(VkInfo* pInfo)
{
	// render pass
	// Image Views
	vkDestroySwapchainKHR(pInfo->hDevice, pInfo->hSwapChain, nullptr);
	vkDestroySurfaceKHR(pInfo->hInstance, pInfo->hSurface, nullptr);
	vkDestroyDescriptorPool(pInfo->hDevice, pInfo->hPool, nullptr);
	vkDestroyDevice(pInfo->hDevice, nullptr);
	vkDestroyInstance(pInfo->hInstance, nullptr);
}

int main([[maybe_unused]] int argc, char* argv[])
{
	SDL_Window* pWindow;
	int width{ 800 };
	int height{ 600 };

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
		return 1;
	}

	pWindow = { SDL_CreateWindow(
			"Application",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			width, height,
			SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI) };

	SDL_Log("%s", ">>>>>>>>>> Starting");

	VkInfo info;
	SetupVulkan(pWindow, &info);

	SDL_Log("%s [%d]", ">>>>>>>>>> Vulkan Loaded", (unsigned long long)info.hInstance);

	while (running) {


		runMainLoop();
	}

	CleanupVulkan(&info);

	SDL_DestroyWindow(pWindow);
	SDL_Quit();

	SDL_Log("%s", "Quitting");

	return 0;
}