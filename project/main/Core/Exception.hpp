#pragma once

#include "String.hpp"

class Exception {
private:
	String message;

public:
	Exception(const char* pMessage) throw() : message(pMessage) {}
	String& Message() { return message; }

	virtual const char* what() const throw() {
		return message.Elements();
	}

	operator const char*() { return message.Elements(); }
};

