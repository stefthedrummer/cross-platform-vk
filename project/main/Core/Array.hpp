#pragma once

#include <SDL.h>
#include "Allocator.hpp"

template<typename TElement>
class Array : public Object, public IArrayView<TElement> {
protected:
	IAllocator* pAllocator;
	TElement* pElements;
	uint32_t size;

	Array(IAllocator* pAllocator, TElement* pElements, uint32_t size)
		: pAllocator(pAllocator), pElements(pElements), size(size) {}

public:
	Array(IAllocator* pAllocator, uint32_t size) : pAllocator(pAllocator), size(size) {
		pElements = pAllocator->Allocate<TElement>(size);
	};
	Array(uint32_t size) : Array(Allocators::Default, size) { };
	Array() : pAllocator(Allocators::Default), pElements(nullptr), size(0) {};

	~Array() {
		pAllocator->Free(pElements);
		new (this) Array(Allocators::Default, nullptr, 0);
	}

	void Invalidate() {
		new (this) Array(Allocators::Default, nullptr, 0);
	}

	inline IAllocator* Allocator() const { return pAllocator; }
	inline uint32_t Size() const { return size; }
	inline TElement* Elements() const { return pElements; }

	operator TElement* () { return pElements; }
	operator void* () { return (void*)pElements; }
};