#pragma once

#include "String.hpp"
#include "Exception.hpp"

class File {
public:

	template<typename T>
	static void Read(String* pFile, List<T>* pOut, uint32_t bufferSize = 1024) {
		SDL_RWops* hFile = SDL_RWFromFile(pFile->Elements(), "r");
		if (hFile == nullptr) throw Exception("File not found");

		Array<T> buffer(bufferSize);
		uint32_t numRead{};

		pOut->Clear();
		while (numRead = SDL_RWread(hFile, (void*)buffer, sizeof(T), buffer.Size())) {
			pOut->AddRange(&buffer, 0, numRead);
		}

		SDL_RWclose(hFile);
	}

	static String ReadString(String* pFile, uint32_t bufferSize = 1024) {
		List<char> content{};
		Read(pFile, &content, bufferSize);

		content.Add('\0');
		//content.Invalidate();

		return String(&content);
	}
};