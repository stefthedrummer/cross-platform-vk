#pragma once

#ifdef  __ANDROID__
extern __inline unsigned int __attribute__((__gnu_inline__, __always_inline__, __artificial__))
__lzcnt(unsigned int __X)
{
	return __builtin_clz(__X);
}
#endif