#pragma once

#include "Intrinsics.hpp"
#include "Allocator.hpp"
#include "Array.hpp"
#include "String.hpp"

template<typename TElement>
class List : public Object, public IArrayView<TElement> {


protected:
	IAllocator* pAllocator;
	TElement* pElements;
	uint32_t capacity;
	uint32_t size;

	List(IAllocator* pAllocator, TElement* pElements, uint32_t capacity, uint32_t size)
		: pAllocator(pAllocator), pElements(pElements), capacity(capacity), size(size) {}

public:
	List(IAllocator* pAllocator, uint32_t capacity)
		: pAllocator(pAllocator), capacity(capacity), size(0) {
		pElements = pAllocator->Allocate<TElement>(size);
	};
	List(uint32_t size)
		: List(Allocators::Default, size) { };
	List()
		: pAllocator(Allocators::Default), pElements(nullptr), capacity(0), size(0) {};

	~List() {
		pAllocator->Free(pElements);
		new (this) List(Allocators::Default, nullptr, 0, 0);
	}

	void Invalidate() {
		new (this) List(Allocators::Default, nullptr, 0, 0);
	}

	inline IAllocator* Allocator() const { return pAllocator; }
	inline uint32_t Size() const { return size; }
	inline TElement* Elements() const { return pElements; }

	void EnsureCapacity(uint32_t ensureCapacity) {
		if (capacity >= ensureCapacity) return;
		uint32_t nextCapacity = SDL_max(1 << (32 - __lzcnt(ensureCapacity - 1)), 4);

		pElements = pAllocator->Reallocate<TElement>(pElements, nextCapacity);
		capacity = nextCapacity;
	}

	void Clear() {
		size = 0;
	}

	void Add(TElement&& element) {
		EnsureCapacity(size + 1);
		pElements[size] = forward<TElement>(element);
		size++;
	}

	void AddRange(Array<TElement>* pArray, uint32_t arrayOffset, uint32_t numElements) {
		EnsureCapacity(size + numElements);
		Memory::Copy(this->Elements() + size, pArray->Elements() + arrayOffset, numElements);
		size += numElements;
	}
};