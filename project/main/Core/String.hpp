#pragma once

#include "Util.hpp"
#include "Array.hpp"

class String : public Array<const char> {
protected:
	String(IAllocator* pAllocator, const char* pChars, uint32_t size) : Array(pAllocator, pChars, size) {}

public:
	String() : String(Allocators::Static, "", 1) {}
	String(const char* pChars) : String(Allocators::Static, pChars, SDL_strlen(pChars) + 1) {}
	String(String* other) : String(other->pAllocator, other->pElements, other->size) {}
	String(String&& other) noexcept : String(other.pAllocator, other.pElements, other.size) {
		new (&other) String();
	};

	String(IArrayView<char>* pArrayView) : String(pArrayView->Allocator(), pArrayView->Elements(), pArrayView->Size()) {
		pArrayView->Invalidate();
	}

	operator const char* () { return Elements(); }
};