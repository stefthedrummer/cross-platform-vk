#pragma once

#include <SDL.h>

#define forward std::forward
#define move std::move
#define byte uint8_t

class Object {
public:
	Object() {}
	Object(const Object& other) = delete;
	virtual ~Object() {};
};

class Memory {
public:
	template<typename T>
	static void Copy(T* pDest, T* pSrc, uint32_t numElements) {
		SDL_memcpy((void*)pDest, (void*)pSrc, numElements * sizeof(T));
	}
};

class IAllocator;

template<typename TElement>
class IArrayView {

public:
	virtual IAllocator* Allocator() const = 0;
	virtual uint32_t Size() const = 0;
	virtual TElement* Elements() const = 0;
	virtual void Invalidate() = 0;
};