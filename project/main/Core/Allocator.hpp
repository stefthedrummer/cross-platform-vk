#pragma once

#include <SDL.h>
#include "Util.hpp"

struct IAllocator {
	virtual void* Allocate(size_t size) = 0;
	virtual void Free(void* pMemory) = 0;
	virtual void* Reallocate(void* pOldMemory, size_t newSize) = 0;

	template<typename TElement>
	inline TElement* Allocate(size_t num = 1) {
		return (TElement*)Allocate(num * sizeof(TElement));
	}

	template<typename TElement>
	inline void Free(TElement* pMemory) {
		return Free((void*)pMemory);
	}

	template<typename TElement>
	inline TElement* Reallocate(TElement* pOldMemory, size_t newNum) {
		return (TElement*)Reallocate((void*)pOldMemory, newNum * sizeof(TElement));
	}
};

namespace Allocators {

	struct DefaultImpl : public IAllocator {
		void* Allocate(size_t size) {
			return SDL_malloc(size);
		}
		void Free(void* pMemory) {
			SDL_free(pMemory);
		}
		void* Reallocate(void* pOldMemory, size_t newNum) {
			void* pNewMemory = SDL_realloc(pOldMemory, newNum);
			return pNewMemory;
		}
	};

	struct StaticImpl : public IAllocator {
		void* Allocate(size_t size) {
			throw "Unsupported Operation";
		}
		void Free(void* pMemory) {}
		void* Reallocate(void* pOldMemory, size_t newNum) {
			throw "Unsupported Operation";
		}
	};

	struct InvalidImpl : public IAllocator {
		void* Allocate(size_t size) {
			throw "Unsupported Operation";
		}
		void Free(void* pMemory) {
			throw "Unsupported Operation";
		}
		void* Reallocate(void* pOldMemory, size_t newNum) {
			throw "Unsupported Operation";
		}
	};

	static DefaultImpl g_DefaultImpl{};
	static StaticImpl g_StaticImpl{};
	static InvalidImpl g_InvalidImpl{};

	static IAllocator* const Default = &g_DefaultImpl;
	static IAllocator* const Static = &g_StaticImpl;
	static IAllocator* const Invalid = &g_InvalidImpl;
}

